"use strict"

let tabInfo = [];
let tmp = {
    valeur : 0,
    zicos_id : 0,
    concerts_id : 0
}

let instrument_select = document.querySelector("#instrument_select");
let zicos_select = document.querySelector("#musicien_select");
let th_table = document.querySelector("#column");
let row = document.querySelector("#rowConcert");
let modal = document.querySelector("#modal");


// request and load tabInfo
fetch('functions/req.php', {
    method: 'POST',
    headers: {'Content-Type':'application/x-www-form-urlencoded'},
    body: 'req=all'
    })
    .then(function(response){
        return response.json();
    })
    .then(function(data){
        tabInfo = data;
        console.log(tabInfo);
        loadInstru();
        fillConcertInTable();   
    });

// load instrument name from tabInfo to select tag
function loadInstru(){
    tabInfo.instru.forEach(function(item){
        let option = document.createElement("option");
        option.setAttribute("value", item.id);
        option.insertAdjacentText('afterbegin', item.nom);
        instrument_select.appendChild(option);
    });
    instrument_select.addEventListener("input", fillSelectMusicians);
};

//loadconcerts from tabInfo to table
function fillConcertInTable(){
    tabInfo.concert.forEach(function(item){
        let tr = document.createElement("tr")
        for(let key in item){
            if(key == 'id')
                tr.setAttribute('concerts_id', item[key])
            else{
                let td = document.createElement("td");
                td.insertAdjacentText('afterbegin', item[key]);
                tr.appendChild(td);
            }
        }
        row.append(tr);
    });
}

//get filtered zicos from selected instrument
function filterData(tab, field, criteria){
    return tab.filter(function(obj){
        if(obj[field] === criteria)
            return obj;
    });
};


//load musician's name to select when an instrument is selected
function fillSelectMusicians(evt){

    //remove musicians from select
    while(zicos_select.lastElementChild.getAttribute("value") != 0){
        zicos_select.removeChild(zicos_select.lastChild);
    }

    // create a select_musicians with all musicians
    if(evt.srcElement.value == 0){
        tabInfo.zicos.forEach(function(item){
            let optionZicos = document.createElement("option");
            optionZicos.setAttribute("value", item.id);
            optionZicos.insertAdjacentText('afterbegin', item.prenom);
            zicos_select.appendChild(optionZicos);
        });
    }

    // create select_musicians with the selected instrument
    else{
        let zicos = filterData(tabInfo.zicos, "instru_id", evt.srcElement.value);
        zicos.forEach(function(item){
            let optionZicos = document.createElement("option");
            optionZicos.setAttribute("value", item.id);
            optionZicos.insertAdjacentText('afterbegin', item.prenom);
            zicos_select.appendChild(optionZicos);
        });
    }
};

// display in the table data for a selected musician
zicos_select.addEventListener("input", function(evt){

    let elemActive = document.querySelectorAll(".active");
    for(let item of elemActive){
        item.remove();
    }

    // find and display musician name in the 'th' tag
    let nom = tabInfo.zicos.find(item => item.id === evt.srcElement.value);
    let th = document.createElement("th");
    th.insertAdjacentText('afterbegin', nom.prenom);
    th.className = "active";
    th_table.appendChild(th);

    let dataZicos = filterData(tabInfo.planning, "id_zicos", evt.srcElement.value);
    console.log(dataZicos);
    dataZicos.forEach(function(item){
        let td = document.createElement("td");
        let valeur = 0;
        if(item.attendance == null){
            valeur = 0;
        }
        else{
            valeur = item.attendance;
        }
        console.log(valeur);
        td.insertAdjacentText('afterbegin', valeur);
        td.className = 'active';
        td.setAttribute("zicos_id", item.id_zicos);
        td.addEventListener("click", getId);
        td.addEventListener("click", setChoiceVisible);
        let rowSelected = searchRow(item.id_concert);
        console.log(rowSelected);
        rowSelected.appendChild(td);
    });
});

function getId(evt){
    tmp.concerts_id = evt.target.parentElement.attributes.concerts_id.value;
    tmp.zicos_id = evt.target.attributes.zicos_id.value;
};


modal.addEventListener("click", setChoiceInvisible);

function setChoiceInvisible(evt){
    tmp.valeur = evt.target.textContent;
    let req = 'concerts_id='+tmp.concerts_id+'&zicos_id='+tmp.zicos_id+'&valeur='+tmp.valeur;
    requestBdd(req);
    modal.style.display = "none";
};

function setChoiceVisible(evt){
    modal.style.display = "block";
};

function requestBdd(req){
    fetch('functions/req.php', {
        method: 'POST',
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        body: req
        })
        .then(function(response){
            if(response.ok){
                return response.json();
            }
            else{
                console.log("erreur requete");
            }
        })
        .then(function(data){
            let resultat = tabInfo.planning.find(item => item.id_concert === data.concerts_id && item.id_zicos === data.zicos_id);
            resultat.attendance = data.valeur;
            searchRow(data.concerts_id).lastChild.textContent = data.valeur;
        })
        .catch(function(error){
            console.log('probleme fetch');
        });
};

// select a row by attribute 'concerts_id'
function searchRow(nb){
    let row = document.querySelector('[concerts_id="'+ nb + '"]');
    return row;
}