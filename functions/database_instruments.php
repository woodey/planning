<?php

/**
 * add instrument
 * @param $instru
 * @return int
 */
function addInstrument($instru)
{
    try{
        $bdd = getConnect();
        if($bdd != false){

            //check if instrument is already used
            $instrus = getInstruments(['nom' => $instru['nom']]);

            if(sizeof($instrus) == 0){
                
                $req = $bdd->prepare('INSERT INTO instruments VALUES (:id, :nom)');
                $req->execute(['id' => $instru['id'],
                              'nom' => $instru['nom']
                            ]);
                $req->closeCursor();
                $status = SQL_SUCCESS;
            }
            else
                $status = SQL_DUPLICATE_ENTRY;
        }
        else
            $status = SQL_ERROR;
    }
    catch(PDOException $e){
        $status = SQL_ERROR;
    }
    return $status;
}


function getInstruments($conditions = [])
{
    try{
        $bdd = getConnect();

        if($bdd != false){

            $query = 'SELECT * FROM instruments';

            //loop on each conditions and build the query
            if(is_array($conditions) && sizeof($conditions) > 0){

                $query .= ' WHERE';
                $where = '';
                $newConditions = [];
                foreach($conditions as $key => $condition){

                    $operator = '=';
                    $finalKey = $key;

                    //check if operator is a difference
                    if(strpos($finalKey, '!=') !== false){
                        $operator = '!=';
                        $finalKey = str_replace(' !=', '', $finalKey);
                    }

                    $newConditions[$finalKey] = $condition;

                    if(!empty($where))
                        $where .= ' AND';
                    $where .= ' '. $finalKey .' '. $operator .' :'. $finalKey; 

                }

                $query .= $where;
                $conditions = $newConditions;
            }

            $req = $bdd->prepare($query);
            $req->execute($conditions);
            $data = $req->fetchAll();
            $req->closeCursor();
        }
        else
            $data = [];
    }
    catch(PDOException $e){
        $data = [];
    }
    return $data;
}

/**
 * 
 */
function updateInstrument($instru)
{
    try{
        $bdd = getConnect();

        if($bdd != false){

            //check if nom is already used
            $instrus = getInstruments(['id !=' => $instru['id'], 'nom' => $instru['nom']]);
            //count if there is an entry found
            if(sizeof($instrus) == 0){

                $query = 'UPDATE instruments SET nom = :nom';
                $param = ['id' => $instru['id'],
                          'nom' => $instru['nom']
                ];

                $query .= ' WHERE id = :id';

                $req = $bdd->prepare($query);
                $req->execute($param);
                $req->closeCursor();

                $status = 'SQL_SUCCESS';
            }
            else{
                $status = SQL_DUPLICATE_ENTRY;
            }
        }
        else{
            $status = SQL_ERROR;
        }
    }
    catch(PDOException $e){
        $status = SQL_ERROR;
    }
    return $status;
}

?>