<?php

/**
 * Check that mandatory fields exist and are not empty
 * @param $requestData
 * @param $mandatoryFields
 * @return bool
 */
function isFormValid($requestData, $mandatoryFields){

    // loop on mandatory fields
    foreach($mandatoryFields as $key => $fieldName){

        // if field has not be submitted
        if(!array_key_exists($fieldName, $requestData)){
            return false;
        }

        // if field is emtpy
        if(empty(trim($requestData[$fieldName]))){
            return false;
        }
    }

    return true;
}
?>