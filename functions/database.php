<?php

function getZico($id){
    try{
        $bdd = getConnect();
        if($bdd != false){
            $req = $bdd->prepare('SELECT musiciens.id AS id,
            musiciens.prenom AS prenom,
            musiciens.email AS email,
            musiciens.job AS job,
            musiciens.actif AS actif,
            musiciens.instru_id AS instru_id 
            FROM musiciens INNER JOIN instruments ON musiciens.instru_id = instruments.id WHERE musiciens.id = :id');
            $req->execute(['id' => $id]);
            $data = $req->fetch(PDO::FETCH_ASSOC);
            $req->closeCursor(); 
        }
        else{
            $data =[];
        }
    }
    catch(PDOException $e){
        $data = [];
    }
    return $data;
}

function getZicos(){
    try{
        $bdd = getConnect();
        if($bdd != false){
            $req = $bdd->prepare('SELECT musiciens.id AS id,
                                musiciens.prenom AS prenom,
                                musiciens.email AS email,
                                musiciens.job AS job,
                                musiciens.actif AS actif,
                                instruments.nom AS instrument

                                FROM musiciens INNER JOIN instruments ON musiciens.instru_id = instruments.id');
            $req->execute();
            $data = $req->fetchAll(PDO::FETCH_ASSOC);
            $req->closeCursor();
        }
        else{
            $data = [];
        }
    }
    catch(PDOException $e){
        $data = [];
    }
    return $data;
}

function addZicos($zico){
    try{
        $bdd = getConnect();
        
        if($bdd != false){
            
            $req = $bdd->prepare('INSERT INTO musiciens VALUES (:id, :prenom, :email, :pwd, :job, :actif, :instru_id)');
            $req->execute(['id' => NULL,
                            'prenom' => $zico['prenom'],
                            'email' => $zico['email'],
                            'pwd' => $zico['pwd'],
                            'job' => $zico['job'],
                            'actif' => $zico['actif'],
                            'instru_id' => $zico['instru_id']
            ]);
            $req->closeCursor();

            $status = SQL_SUCCESS;
        }
        else{
            $status = SQL_DUPLICATE_ENTRY;
        }
    }
    catch(PDOException $e){
        $status = SQL_ERROR;
    }
    return $status;
}

function updateZico($zico){
    try{
        $bdd = getConnect();
        if($bdd != false){

            $query = 'UPDATE musiciens SET 
                        prenom = :prenom,
                        email = :email,
                        instru_id = :instru_id,
                        job = :job,
                        actif = :actif';
            $param = ['id' => $zico['id'],
                        'prenom' => $zico['prenom'],
                        'email' => $zico['email'],
                        'instru_id' => $zico['instru_id'],
                        'job' => $zico['job'],
                        'actif' => $zico['actif'],
                        ];
            $query .= ' WHERE id = :id';

            $req = $bdd->prepare($query);
            $req->execute($param);
            $req->closeCursor();
            $status = SQL_SUCCESS;
        }
        else{
            $status = SQL_ERROR;
        }
    }
    catch(PDOException $e){
        $status = SQL_ERROR;
    }
    return $status;
}

?>