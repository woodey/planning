<?php

function addConcert($concert)
{
    try{
        $bdd = getConnect();
        if($bdd != false){
            
            $bdd->beginTransaction();

            //check if concert is already used
            $concerts = getConcerts(['jour' => $concert['jour']]);
            
            if(sizeof($concerts) == 0){
                
                $req = $bdd->prepare('INSERT INTO concerts VALUES (:id, :jour, :lieu, :sorte, :remarque)');
                $req->execute(['id' => $concert['id'],
                              'jour' => $concert['jour'],
                              'lieu' => $concert['lieu'],
                              'sorte' => $concert['sorte'],
                              'remarque' => $concert['remarque']
                            ]);
                // get the id of the new event
                $req = $bdd->prepare('SELECT id FROM concerts WHERE jour = :jour AND lieu = :lieu');
                $req->execute(['jour' => $concert['jour'],
                                'lieu' => $concert['lieu']
                                ]);
                $concert_id = $req->fetch(PDO::FETCH_ASSOC);

                // get all id of musiciens
                $req = $bdd->prepare('SELECT id FROM musiciens');
                $req->execute();
                $zicos = $req->fetchAll(PDO::FETCH_ASSOC);

                //set new tuple in presence
                $req = $bdd->prepare('INSERT INTO presences VALUES (:id_concert, :id_zicos, :attendance)');
                foreach($zicos as $key => $zico){
                    $req->execute(['id_concert' => $concert_id['id'],
                                    'id_zicos' => $zico['id'],
                                    'attendance' => NULL
                                    ]);
                }
                $bdd->commit();
                $req->closeCursor();

                $status = SQL_SUCCESS;
            }
            else
                $status = SQL_DUPLICATE_ENTRY;
        }
        else
            $status = SQL_ERROR;
    }
    catch(PDOException $e){
        $bdd->rollback();
        $status = SQL_ERROR;
    }
    return $status;
}

/**
 * get concerts
 * @return concerts array || [empty]
 */
function getConcerts($conditions = [])
{
    try{
        $bdd = getConnect();

        if($bdd != false){

            $query = 'SELECT * FROM concerts';

            //loop on each conditions and build the query
            if(is_array($conditions) && sizeof($conditions) > 0){

                $query .= ' WHERE';
                $where = '';
                $newConditions = [];
                foreach($conditions as $key => $condition){

                    $operator = '=';
                    $finalKey = $key;

                    //check if operator is a difference
                    if(strpos($finalKey, '!=') !== false){
                        $operator = '!=';
                        $finalKey = str_replace(' !=', '', $finalKey);
                    }

                    $newConditions[$finalKey] = $condition;

                    if(!empty($where))
                        $where .= ' AND';
                    $where .= ' '. $finalKey .' '. $operator .' :'. $finalKey; 

                }

                $query .= $where;
                $conditions = $newConditions;
            }

            $req = $bdd->prepare($query);
            $req->execute($conditions);
            $data = $req->fetchAll(PDO::FETCH_ASSOC);
            $req->closeCursor();
        }
        else
            $data = [];
    }
    catch(PDOException $e){
        $data = [];
    }
    return $data;
}


function updateConcert($concert){
    try{
        $bdd = getConnect();
        if($bdd != false){

            $query = 'UPDATE concerts SET 
                        jour = :jour,
                        lieu = :lieu,
                        sorte = :sorte,
                        remarque = :remarque';
            $param = ['id' => $concert['id'],
                        'jour' => $concert['jour'],
                        'lieu' => $concert['lieu'],
                        'sorte' => $concert['sorte'],
                        'remarque' => $concert['remarque']
                        ];
            $query .= ' WHERE id = :id';

            $req = $bdd->prepare($query);
            $req->execute($param);
            $req->closeCursor();
            $status = SQL_SUCCESS;
        }
        else{
            $status = SQL_ERROR;
        }
    }
    catch(PDOException $e){
        $status = SQL_ERROR;
    }
    return $status;
}


function deleteConcert($concert){
    try{
        $bdd = getConnect();

        if($bdd != false){
            
            $bdd->beginTransaction();

            // first delete tuples from presences (foreign_key) by id_concert
            $req = $bdd->prepare('DELETE FROM presences WHERE id_concert = :id');
            $req->execute(['id' => $concert['id']]);

            // second delete tuple from concerts by id
            $req = $bdd->prepare('DELETE FROM concerts WHERE id = :id');
            $req->execute(['id' => $concert['id']]);

            $bdd->commit();
            $req->closeCursor();

            $status = SQL_SUCCESS;
        }
        else{
            $status = SQL_ERROR;
        }
    }
    catch(PDOException $e){
        $status = SQL_ERROR;
    }
    return $status;
}
?>