<?php

session_start();

/**
 * destroy variable of the session
 * @param $name 
 */
function unsetSession($name){

    unset($_SESSION[$name]);
}

/**
 * read a session variable
 * @param $name
 * @return something || null
 */
function readSession($name){

    return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
}

/**
 * write in session
 * @param $name
 * @param $value
 */
function writeSession($name, $value){

    $_SESSION[$name] = $value;
}

/**
 * get the value of message of the session
 * @return $message
 */
function getMessageSession(){

    $message = '';
    $session = readSession('message');
    if(!is_null($session)){
        $message = $session;
        unsetSession('message');
    }
    return $message;
}
?>