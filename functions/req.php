<?php

require_once('../database/connection.php');

if(isset($_POST['concerts_id'])){
    $data = $_POST;
    $query = 'UPDATE presences SET attendance = :attendance WHERE id_concert = :id_concert AND id_zicos = :id_zicos';
    $param = (['id_concert' => $data['concerts_id'],
                'id_zicos' => $data['zicos_id'],
                'attendance' => $data['valeur']
                ]);
    $bdd = getConnect();
    $req = $bdd->prepare($query);
    $req->execute($param);
    echo json_encode($data);
}
else{

    $data = [];

    $bdd = getConnect();
    $query = 'SELECT * FROM instruments';
    $req = $bdd->prepare($query);
    $req->execute();
    $data['instru'] = $req->fetchAll(PDO::FETCH_ASSOC);


    $query = 'SELECT * FROM musiciens';
    $req = $bdd->prepare($query);
    $req->execute();
    $data['zicos'] = $req->fetchAll(PDO::FETCH_ASSOC);

    $query = 'SELECT * FROM presences';
    $req = $bdd->prepare($query);
    $req->execute();
    $data['planning'] = $req->fetchAll(PDO::FETCH_ASSOC);

    $query = 'SELECT * FROM concerts';
    $req = $bdd->prepare($query);
    $req->execute();
    $data['concert'] = $req->fetchAll(PDO::FETCH_ASSOC);

    $req->closeCursor();

    echo json_encode($data);
}

?>