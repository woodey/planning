<?php

/**
 * redirect toward given page
 * @param $page
 */
function redirect($page){
    header('Location: '. $page);
    exit();
}

/**
 * get formatter messages
 * @param $message
 * @param String $type
 * @return String
 */
function formatMessage($message, $type = ' normal'){
    switch($type){
        case 'success':
            $class = 'txt-green';
            break;
        case 'warning':
            $class = 'txt-green';
            break;
        case 'error':
            $class = 'txt-red';
            break;
        default:
            $class = 'txt-black';
            break;
    }
    return '<div class="message '. $class .'">'. $message .'</div>';
}

?>