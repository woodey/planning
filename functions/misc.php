<?php

/**
 * convert special characters to HTML entities
 * @param $value
 * @return String
 */
function h($value){
    return htmlspecialchars($value);
}

?>