<?php



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Accueil</title>
    <style>
        #modal{
            width: 50px;
            background-color: #ccc;
            display: none;
        }
        #modal p{
            margin: 0;
            text-align: center;
            border: 1px solid black;
        }
        table{
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid black;
        }
    </style>
</head>

<body>

    <nav>
        <ul>
            <li><a href="concert/concerts_list.php">concerts</a></li>
            <li><a href="musicien/musiciens_list.php">musiciens</a></li>
            <li><a href="instrument/instrument_list.php">instruments</a></li>
        </ul>
    </nav>
    
    <select id="instrument_select" name="instrument_select">
        <option value="0">tous</option>
    </select>
        
    <select id="musicien_select">
        <option value="0">---</option>
    </select>

    <table>
        <thead>
            <tr id="column">
                <th>date</th>
                <th>lieu</th>
                <th>type</th>
                <th>remarque</th>
            </tr>
        </thead>
        <tbody id="rowConcert"></tbody>
    </table>

    <div id="modal">
        <p>0</p>
        <p>1</p>
        <p>2</p>
        <p>3</p>
    </div>

    <script src="JS/planning.js"></script>

</body>
</html>