<?php

require_once('../actions/musiciens_update.php');

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Modifier un musicien</title>
</head>

<body>

    <?php echo getMessageSession(); ?>

    <a href="musiciens_list.php">retour à la liste des musiciens</a>

    <h1>Modifier un musicien</h1>

    <form action="musiciens_update.php?id=<?php echo $_GET['id']; ?>" method="POST">

        <label for="">prénom</label><br>
        <input type="text" name="prenom" value="<?php echo $prenom; ?>" required><br>

        <label for="">email</label><br>
        <input type="email" name="email" value="<?php echo $email; ?>" required><br>

        <label for="">mot de passe</label><br>
        <input type="password" name="pwd" value="<?php echo $pwd; ?>"><br>

        <label for="">instrument</label><br>
        <select name="instru_id">
            <option value=""></option>
            <?php foreach($instruments as $key => $instrument) : ?>
            <option value="<?php echo $instrument['id']; ?>"<?php if($id_instru == $instrument['id']) echo ' selected'; ?>><?php echo $instrument['nom']; ?></option>
            <?php endforeach; ?>
        </select><br>

        <label>actif</label><br>
        <select name="actif" required>
            <option value=""></option>
            <option value="1" <?php echo $actifSelected1; ?>>oui</option>
            <option value="2" <?php echo $actifSelected2; ?>>non</option>
        </select><br>

        <label>role</label><br>
        <select name="job" required>
            <option value=""></option>
            <option value="1" <?php echo $roleSelected1; ?>>admin</option>
            <option value="2" <?php echo $roleSelected2; ?>>user</option>
        </select><br>

        <button name="btn-updateZico">modifier</button>
    </form>
</body>
</html>