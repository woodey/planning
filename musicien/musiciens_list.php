<?php
require_once('../actions/musiciens_read.php');

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Liste des musiciens</title>
</head>
<body>
    
    <?php echo getMessageSession(); ?>

    <a href="../general.php">Retour Accueil</a>

    <h1>Liste des musiciens</h1>

    <a href="musiciens_add.php">ajouter un musicien</a>

    <?php if(sizeof($zicos) > 0) : ?>
    <table>
        <thead>
            <tr>
                <th>prénom</th>
                <th>email</th>
                <th>instrument</th>
                <th>actif</th>
                <th>job</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($zicos as $key => $zico) : ?>
            <tr>
                <td><?php echo h($zico['prenom']); ?></td>
                <td><?php echo h($zico['email']); ?></td>
                <td><?php echo h($zico['instrument']); ?></td>
                <td><?php echo ACTIF[$zico['actif']]; ?></td>
                <td><?php echo JOB[$zico['job']]; ?></td>
                <td><a href="musiciens_update.php?id=<?php echo $zico['id']; ?>">modifier</a></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <?php else : ?>
    <p>Pas de musicien trouvé</p>
    <?php endif; ?>

</body>
</html>