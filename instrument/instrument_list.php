<?php

require_once('../actions/instruments_read.php');

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Liste des instruments</title>
</head>
<body>
    <?php echo getMessageSession(); ?>

    <a href="../general.php">retour à la page de gestion</a>

    <h1>Liste des instruments</h1>

    <a href="instruments_add.php">ajouter</a>

    <?php if(sizeof($instruments) > 0) : ?>
        <table>
            <thead>
                <tr>
                    <th>instrument</th>
                    <th>actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($instruments as $key => $instru) : ?>
                <tr>
                    <td><?php echo $instru['nom']; ?></td>
                    <td>
                        <a href="instruments_update.php?id=<?php echo $instru['id']; ?>">modifier</a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else : ?>
        <p>Aucun instrument trouvé</p>
    <?php endif; ?>

</body>
</html>