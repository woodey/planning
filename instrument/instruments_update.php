<?php

require_once('../actions/instruments_update.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>mise à jour d'un instrument</title>
</head>
<body>

    <?php echo getMessageSession(); ?>

    <h1>modifier un instrument</h1>

    <a href="instrument_list.php">revenir à la liste des instruments</a>

    <form action="instruments_update.php?id=<?php echo $_GET['id']; ?>" method="POST">
        <label>instrument</label>
        <input type="text" name="nom" value="<?php echo $instruNom; ?>" required>
        <button name="btn-updateInstrument">mettre à jour</button>
    </form>

</body>
</html>