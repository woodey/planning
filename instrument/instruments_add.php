<?php

require_once('../actions/instruments_add.php');

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ajouter un instrument</title>
</head>

<body>
    <?php echo getMessageSession(); ?>

    <a href="instruments.php">revenir à la liste des instruments</a>

    <h1>Ajouter un instrument</h1>

    <form action="instruments_add.php" method="POST">
        <label>instrument : </label>
        <input type="text" name="nom" value="<?php echo $instruNom; ?>" required>
        <button name="btn-addInstrument">ajouter</button>
    </form>

</body>
</html>