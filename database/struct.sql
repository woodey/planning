create table instruments (
	id int primary key auto_increment,
	nom varchar(50)
	)
	ENGINE = InnoDB;


create table musiciens (
	id int primary key auto_increment,
	prenom varchar(50) not null,
    email varchar(100),
    pwd varchar(255),
    job int,
    actif int,
	instru_id int
	)
	ENGINE = InnoDB;

alter table musiciens
	add constraint fk_id_instrus
	foreign key (instru_id)
	references instruments(id);



create table concerts (
	id int primary key auto_increment,
	jour date,
	lieu varchar(50),
	sorte varchar(50),
	remarque varchar(50)
	)
	ENGINE = InnoDB;



create table presences (
	id_concert int,
	id_zicos int,
	attendance int,
	primary key(id_concert, id_zicos)
	)
	ENGINE = InnoDB;

alter table presences
	add constraint fk_id_concert
	foreign key (id_concert)
	references concerts(id);

alter table presences
	add constraint fk_id_zicos
	foreign key (id_zicos)
	references musiciens(id);