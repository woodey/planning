<?php

define('DB_HOST', 'localhost');
define('DB_DATABASE', 'planning');
define('DB_LOGIN', 'root');
define('DB_PWD', 'woodey');
define('PORT', '3306');

define('SQL_ERROR', -2);
define('SQL_DUPLICATE_ENTRY', -1);
define('SQL_SUCCESS', 0);

/**
 * get database connection
 * @return bool || PDO
 */
function getConnect()
{
    try{
        $bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_DATABASE.';charset=utf8;port='.PORT,
                        DB_LOGIN,
                        DB_PWD,
                        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    }
    catch(PDOException $e){
        return false;
    }

    return $bdd;
}

?>