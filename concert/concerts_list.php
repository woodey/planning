<?php

require_once('../actions/concerts_read.php');

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>liste des dates</title>
</head>
<body>

    <?php echo getMessageSession(); ?>

    <a href="../general.php">retour à la page de gestion</a>

    <h1>Liste des dates</h1>

    <a href="concerts_add.php">ajouter</a>

    <?php if(sizeof($concerts) > 0) : ?>
        <table>
            <thead>
                <tr>
                    <th>date</th>
                    <th>lieu</th>
                    <th>type</th>
                    <th>remarque</th>
                    <th>actions</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach($concerts as $key => $concert) : ?>
                    <tr>
                        <td><?php echo $concert['jour']; ?></td>
                        <td><?php echo $concert['lieu']; ?></td>
                        <td><?php echo $concert['sorte']; ?></td>
                        <td><?php echo $concert['remarque']; ?></td>
                        <td>
                            <a href="concerts_update.php?id=<?php echo $concert['id']; ?>">modifier</a>
                            -
                            <a href="concerts_delete.php?id=<?php echo $concert['id']; ?>">supprimer</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else : ?>
        <p>Aucune date trouvée</p>
    <?php endif; ?>

</body>
</html>