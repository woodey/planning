<?php

require_once('../actions/concerts_delete.php');

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Supprimer un événement</title>
</head>

<body>

    <?php echo getMessageSession(); ?>

    <a href="concerts_list.php">revenir à la liste des concerts</a>

    <h1>supprimer un événement</h1>

    <form action="concerts_delete.php?id=<?php echo $concert['id']; ?>" method="POST">
        <input type="hidden" name="id" value="<?php echo $concert['id']; ?>">
        <p>Sûr de supprimer l'événement du <?php echo $concert['jour']?> à <?php echo $concert['lieu']; ?> ?</p>
        <button name="btn-deleteConcert">Supprimer</button>
    </form>

</body>
</html>