<?php

require_once('../actions/concerts_add.php');

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ajouter un concert</title>
</head>

<body>
    <?php echo getMessageSession(); ?>

    <a href="concerts_list.php">revenir à la liste des concerts</a>

    <h1>Ajouter un concert</h1>

    <form action="concerts_add.php" method="POST">

        <label>Jour</label><br>
        <input type="date" name="jour" value="<?php echo $jour; ?>" required><br>

        <label>lieu</label><br>
        <input type="text" name="lieu" value="<?php echo $lieu; ?>" required><br>
        
        <label>que - quoi</label><br>
        <input type="text" name="sorte" value="<?php echo $quequoi; ?>"><br>

        <label>remarque</label><br>
        <input type="text" name="remarque" value="<?php echo $remarque?>"><br>

        <button name="btn-addConcert">Ajouter</button>
    </form>
</body>
</html>