<?php

require_once('../database/connection.php');
require_once('../functions/database_concerts.php');
require_once('../functions/form.php');
require_once('../functions/message.php');
require_once('../functions/user.php');
require_once('../functions/misc.php');
require_once('../functions/session.php');

//check if user is connected
//check if url is correct


if(isset($_POST['btn-updateConcert'])){

     //list mandatory fields
    $mandatoryFields = ['jour', 'lieu'];

    if(isFormValid($_POST, $mandatoryFields)){

        //todo check date
            $concert = [];
            $concert['id'] = $_GET['id'];
            $concert['jour'] = $_POST['jour'];
            $concert['lieu'] = $_POST['lieu'];
            $concert['sorte'] = $_POST['sorte'];
            $concert['remarque'] = $_POST['remarque'];

            $status = updateConcert($concert);

            if($status == SQL_SUCCESS){
                writeSession('message', formatMessage('événement modifié'));
                redirect('concerts_list.php');
            }
            elseif($status == SQL_DUPLICATE_ENTRY){
                writeSession('message', formatMessage('date déjà utilisé', 'warning'));
            }
            else{
                writeSession('message', formatMessage('Erreur SQL, réessayez', 'error'));
            }
    }
    else{
        writeSession('message', formatMessage('Champs invalides', 'warning'));
    }
}

$concerts = getConcerts(['id' => $_GET['id']]);
$concert = (sizeof($concerts) > 0) ? reset($concerts) : NULL;

$jour = isset($concert['jour']) ? $concert['jour'] : 'yyyy/mm/dd';
$lieu = isset($concert['lieu']) ? h($concert['lieu']) : '';
$quequoi = isset($concert['sorte']) ? h($concert['sorte']) : '';
$remarque = isset($concert['remarque']) ? h($concert['remarque']) : '';

?>