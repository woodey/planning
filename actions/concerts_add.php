<?php

require_once('../database/connection.php');
require_once('../functions/database_concerts.php');
require_once('../functions/form.php');
require_once('../functions/message.php');
require_once('../functions/session.php');

// form is submitted
if(isset($_POST['btn-addConcert'])){

    //list mandatory fields
    $mandatoryFields = ['jour', 'lieu'];

    //check form validity
    if(isFormValid($_POST, $mandatoryFields)){

        $concert = [];
        $concert['id'] = NULL;
        $concert['jour'] = $_POST['jour'];
        $concert['lieu'] = $_POST['lieu'];
        $concert['sorte'] = $_POST['sorte'];
        $concert['remarque'] = $_POST['remarque'];
        
        //add $concert in database
        $status = addConcert($concert);
        if($status == SQL_SUCCESS){
            writeSession('message', formatMessage('événement ajouté', 'success'));
            redirect('concerts_list.php');
        }
        else if($status == SQL_DUPLICATE_ENTRY){
            writeSession('message', formatMessage('date déjà utillisé', 'warning'));
        }
        else{
            writeSession('message', formatMessage('Erreur bdd', 'error'));
        }
    }
    else{
        writeSession('message', formatMessage('champs obligatoire', 'error'));
    }
}

$jour = isset($_POST['jour']) ? $_POST['jour'] : '';
$lieu = isset($_POST['lieu']) ? $_POST['lieu'] : '';
$quequoi = isset($_POST['sorte']) ? $_POST['sorte'] : '';
$remarque = isset($_POST['remarque']) ? $_POST['remarque'] : '';

?>