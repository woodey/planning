<?php

require_once('../database/connection.php');
require_once('../functions/database_instruments.php');
require_once('../functions/form.php');
require_once('../functions/message.php');
require_once('../functions/session.php');

//check if user is connected
//check if url is correct

if(isset($_POST['btn-updateInstrument'])){

    //list mandatory fields
    $mandatoryFields = ['nom'];
    if(isFormValid($_POST, $mandatoryFields)){
        $instru = [];
        $instru['id'] = $_GET['id'];
        $instru['nom'] = $_POST['nom'];

        $status = updateInstrument($instru);

        if($status == SQL_SUCCESS){
            writeSession('message', formatMessage('instrument modifié', 'success'));
            redirect('instrument_list.php');
        }
        elseif($status == SQL_DUPLICATE_ENTRY){
            writeSession('message', formatMessage('instrument déjà existant', 'warning'));
        }
        else{
            writeSession('message', formatMessage('Erreur SQl, veuillez ressayer'));
        }
    }
    else{
        writeSession('message', formatMessage('Champs obligatoires', 'warning'));
    }

    $instru = $_POST;

}
else{
    $instrus = getInstruments(['id' => $_GET['id']]);
    $instru = (sizeof($instrus) > 0) ? reset($instrus) : null;

    if(empty($instru)){
        writeSession('message', formatMessage('instrument invalide', 'error'));
        redirect('instrument_list.php');
    }
}
$instruNom = isset($instru['nom']) ? $instru['nom'] : '';
echo $instruNom;
?>