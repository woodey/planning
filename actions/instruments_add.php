<?php

require_once('../database/connection.php');
require_once('../functions/database_instruments.php');
require_once('../functions/form.php');
require_once('../functions/message.php');
require_once('../functions/session.php');


// form is submitted
if(isset($_POST['btn-addInstrument'])){

    //list mandatory fields
    $mandatoryFields = ['nom'];

    //check form validity
    if(isFormValid($_POST, $mandatoryFields)){

        $instru = [];
        $instru['id'] = NULL;
        $instru['nom'] = $_POST['nom'];

        //add $instru in database
        $status = addInstrument($instru);
        
        if($status == SQL_SUCCESS){
            writeSession('message', formatMessage('instrument ajouté', 'success'));
            redirect('instrument_list.php');
        }
        else if($status == SQL_DUPLICATE_ENTRY){
            writeSession('message', formatMessage('instrument déjà utillisé', 'warning'));
        }
        else{
            writeSession('message', formatMessage('Erreur bdd', 'error'));
        }
    }
    else{
        writeSession('message', formatMessage('champs obligatoire', 'error'));
    }

}

$instruNom = isset($instrument['nom']) ? $instrument['nom'] : '';

?>