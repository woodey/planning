<?php

require_once('../database/connection.php');
require_once('../functions/database_concerts.php');
require_once('../functions/form.php');
require_once('../functions/message.php');
require_once('../functions/user.php');
require_once('../functions/misc.php');
require_once('../functions/session.php');

if(isset($_POST['btn-deleteConcert'])){
    
    $concert['id'] = $_POST['id'];

    $status = deleteConcert($concert);

    if ($status == SQL_SUCCESS) {

        writeSession('message', formatMessage('événement supprimé', 'success'));
        redirect('concerts_list.php');

    }
    else{
        writeSession('message', formatMessage('Erreur SQL, veuillez reessayer', 'error'));
    }
}

// manually arrived on page
//checkUrlUser($_GET);


$concerts = getConcerts(['id' => $_GET['id']]);
$concert = (sizeof($concerts) > 0) ? reset($concerts) : NULL;

?>