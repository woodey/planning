<?php

require_once('../database/connection.php');
require_once('../functions/database.php');
require_once('../functions/database_instruments.php');
require_once('../functions/form.php');
require_once('../functions/message.php');
require_once('../functions/user.php');
require_once('../functions/misc.php');
require_once('../functions/session.php');

if(isset($_POST['btn-updateZico'])){

    $mandatoryFields = ['prenom', 'email', 'job', 'actif', 'instru_id'];

    if(isFormValid($_POST, $mandatoryFields)){

        if(array_key_exists($_POST['job'], JOB) && array_key_exists($_POST['actif'], ACTIF)){
            //if(isEmeilValid($_POST['email]) !== false){}
                $zico =[];
                $zico['id'] = $_GET['id'];
                $zico['prenom'] = $_POST['prenom'];
                $zico['email'] = $_POST['email'];
                $zico['pwd'] = empty($_POST['pwd']) ? $_POST['pwd'] : '';
                $zico['job'] = $_POST['job'];
                $zico['actif'] = $_POST['actif'];
                $zico['instru_id'] = $_POST['instru_id'];

                $status = updateZico($zico);

                if($status == SQL_SUCCESS){
                    writeSession('message', formatMessage('utilisateur modifié'));
                    redirect('musiciens_list.php');
                }
                elseif($status == SQL_DUPLICATE_ENTRY){
                    writeSession('message', formatMessage('email déjà utilisé', 'warning'));
                }
                else{
                    writeSession('message', formatMessage('Erreur SQL, réessayez', 'error'));
                }

        }
        else{
            writeSession('message', formatMessage('Champs invalides', 'warning'));var_dump($_POST);
        }

        //keep form data for display if login failed
        $zico = $_POST;
    }
    else{
        writeSession('message', formatMessage('Champs obligatoire', 'warning'));
    }
}

$instruments = getInstruments();
$zico = getZico($_GET['id']);

$prenom = isset($zico['prenom']) ? h($zico['prenom']) : '';
$email = isset($zico['email']) ? h($zico['email']) : '';
$pwd = isset($zico['pwd']) ? h($zico['pwd']) : '';
$roleSelected1 = isset($zico['job']) && $zico['job'] == '1' ? 'selected="selected"' : '';
$roleSelected2 = isset($zico['job']) && $zico['job'] == '2' ? 'selected="selected"' : '';
$actifSelected1 = isset($zico['actif']) && $zico['actif'] == '1' ? 'selected="selected"' : '';
$actifSelected2 = isset($zico['actif']) && $zico['actif'] == '2' ? 'selected="selected"' : '';
$id_instru = isset($zico['instru_id']) ? h($zico['instru_id']) : '';

?>