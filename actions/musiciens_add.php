<?php

require_once('../database/connection.php');
require_once('../functions/database.php');
require_once('../functions/database_instruments.php');
require_once('../functions/session.php');
require_once('../functions/message.php');
require_once('../functions/form.php');

if(isset($_POST['btn-addZicos'])){
    
    $mandatoryFields = ['prenom', 'email', 'pwd', 'instru_id', 'actif', 'job'];
    
    if(isFormValid($_POST, $mandatoryFields)){
    // TODO tester validité de 'actif' et 'job' ; hasher mot de passe
    // tester si donnees pas déjà enregistrer
        $zico = [];
        $zico['id'] = NULL;
        $zico['prenom'] = $_POST['prenom'];
        $zico['email'] = $_POST['email'];
        $zico['pwd'] = $_POST['pwd'];
        $zico['instru_id'] = $_POST['instru_id'];
        $zico['actif'] = $_POST['actif'];
        $zico['job'] = $_POST['job'];
        
        $status = addZicos($zico);
        echo "yo";
        if($status == SQL_SUCCESS){
            writeSession('message', formatMessage('musicien ajouté', 'success'));
            redirect('musiciens_list.php');
        }
        else if($status == SQL_DUPLICATE_ENTRY){
            writeSession('message', formatMessage('nom musicien déjà utilisé', 'warning'));
        }
        else{
            writeSession('message', formatMessage('Erreur bdd', 'error'));
        }
    }
    else{
        writeSession('message', formatMessage('champs obligatoire', 'error'));
    }
}

$prenom = isset($zico['prenom']) ? $zico['prenom'] : '';
$email = isset($zico['email']) ? $zico['email'] : '';
$pwd = isset($zico['pwd']) ? $zico['pwd'] : '';
$instru_id = isset($zico['instru_id']) ? $zico['instru_id'] : '';
$actif = isset($zico['actif']) ? $zico['actif'] : '';
$categorie = isset($zico['job']) ? $zico['job'] : '';

$instruments = getInstruments();
?>